function fetchFilms() {
    fetch('https://ajax.test-danit.com/api/swapi/films')
        .then(response => response.json())
        .then(data => {
            const filmsList = document.getElementById('films-list');
            data.forEach(film => {
                const filmElement = document.createElement('div');
                filmElement.classList.add('film'); // Додамо клас для стилізації

                const filmHeader = document.createElement('h2');
                filmHeader.textContent = `Episode ${film.episodeId}: ${film.name}`;
                filmElement.appendChild(filmHeader);

                const filmDescription = document.createElement('p');
                filmDescription.textContent = film.openingCrawl;
                filmElement.appendChild(filmDescription);

                const filmContent = document.createElement('div');
                filmContent.classList.add('content'); // Клас для контенту, який буде анімований
                filmContent.style.display = 'none'; // Початково ховаємо контент персонажів

                fetchCharacters(film.characters, filmContent); // Передача елементу контенту для персонажів

                filmElement.appendChild(filmContent);
                filmsList.appendChild(filmElement);


                filmHeader.addEventListener('click', () => {
                    filmContent.style.display = filmContent.style.display === 'none' ? 'block' : 'none';
                });
            });
        })
        .catch(error => console.error('Error fetching films:', error));
}

function fetchCharacters(characters, filmContent) {
    Promise.all(characters.map(characterURL =>
        fetch(characterURL)
            .then(response => response.json())
    ))
        .then(charactersData => {
            const charactersList = document.createElement('ul');
            charactersData.forEach(character => {
                const characterItem = document.createElement('li');
                characterItem.textContent = character.name;
                charactersList.appendChild(characterItem);
            });
            filmContent.appendChild(charactersList);
        })
        .catch(error => console.error('Error fetching characters:', error));
}

fetchFilms();
